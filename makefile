# 
# The MIT License (MIT)
#  
# Copyright (c) 2014 Werner Strydom
#  
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#  
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#  
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# 

CC		= gcc
CFLAGS	= -g -ansi
SRC=$(wildcard src/*.c)   
HEADERS=$(wildcard src/*.h)
LEXER=$(wildcard src/*.l)
PARSER=$(wildcard src/*.y)  
GENERATED_SRC=$(addprefix obj/,$(notdir $(LEXER:.l=.c) $(PARSER:.y=.c)))

all: bin/grammatika

bin/grammatika: configure $(SRC) $(GENERATED_SRC) makefile 
	$(CC) $(CFLAGS) $(SRC) $(GENERATED_SRC) -o $@ -I src -I obj

configure:
	mkdir -p obj bin 

obj/lexer.h obj/lexer.c: src/lexer.l 
	flex  --outfile=obj/lexer.c --header-file=obj/lexer.h src/lexer.l
 
obj/parser.h obj/parser.c:	src/parser.y obj/lexer.c obj/lexer.h
	bison src/parser.y -o obj/parser.c -d -v  
 
clean:
	rm -Rf obj bin 

.PHONY: all clean 