#include <stdio.h> 
#include <stdarg.h>
#include "parser.h"
#include "lexer.h"

int yyerror(YYLTYPE * yylloc_param, void *scanner, const char *s)
{
    printf("*** Lexical Error %s %d.%d-%d.%d\n", 
    		s, 
         yylloc_param->first_line, 
         yylloc_param->first_column, 
         yylloc_param->last_line, 
         yylloc_param->last_column);

    return 0;
}