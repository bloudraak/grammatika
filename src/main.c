/*
# 
# The MIT License (MIT)
#  
# Copyright (c) 2014 Werner Strydom
#  
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#  
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#  
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# 
*/

#include <stdio.h> 
#include <string.h>
#include "parser.h"
#include "lexer.h"
#include "ast.h"
#include "generator.h"

int main(int argc, char** argv)
{
	Language* language = NULL;
	yyscan_t scanner;
	YY_BUFFER_STATE state = NULL;
	FILE* in = NULL;
	int result = 0;
	int expected = 0;

	if (argc != 2)
	{
		fprintf(stderr, "Usage: %s <grammar> <options> <output>\n", argv[0]);
		return (1);
	}

	if(!(in = fopen(argv[1], "r"))) 
	{
		perror(argv[1]);
		return (3); 
	}

	if (yylex_init(&scanner)) 
	{
		exit(2);
	}

	yy_switch_to_buffer(yy_create_buffer(in, YY_BUF_SIZE, scanner), scanner);
	result = yyparse(&language, scanner);
	yy_delete_buffer(state, scanner);
	yylex_destroy(scanner);

	if (NULL != language)
	{
		printf("Generating Parser for Language \"%s\"\n", language->name);
		Generate(language, "Generator.cs");
	}

	if (NULL != language)
	{
		FreeLanguage(language);
	}
	
	if (NULL != in)
	{
		fclose(in);
	}

  	return(result);
}