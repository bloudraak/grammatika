#ifndef __GRAMMATIKA_GENERATOR_H__
#define __GRAMMATIKA_GENERATOR_H__

#include "ast.h"

void Generate(struct Language* language, const char* path);

#endif