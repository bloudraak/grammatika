#ifndef __GRAMMATIKA_LANGUAGE_H__
#define __GRAMMATIKA_LANGUAGE_H__

typedef struct Language {
	char* name;
} Language;

Language* CreateLanguage(const char* name);
void FreeLanguage(Language* language);

void SetLanguageName(Language* language, const char* name);
const char* GetLanguageName(Language* language);

#endif