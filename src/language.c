#include "language.h"
#include <stdlib.h>
#include <string.h>

Language* CreateLanguage(const char* name)
{
	Language *result = NULL;
	
	if (NULL == name)
	{
		return NULL;
	}

	result = (Language*)malloc(sizeof(Language));
	if (NULL == result)
	{
		return NULL;
	}
	result->name = strdup(name);
	return result;
}

void FreeLanguage(Language* language)
{
	if (NULL == language)
	{
		return;
	}

	if (NULL != language->name)
	{
		free(language->name);
	}

	free(language);
}

void SetLanguageName(Language* language, const char* name)
{
	if (NULL == language)
	{
		return;
	}

	if (NULL == name)
	{
		return;
	}

	if (0 == strlen(name))
	{
		return;
	}

	if (language->name)
	{
		free(language->name);
	}

	language->name = strdup(name);
}

const char* GetLanguageName(Language* language)
{
	if (NULL == language)
	{
		return NULL;
	}

	return language->name;
}
