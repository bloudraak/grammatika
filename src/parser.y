%{
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "parser.h"
#include "lexer.h"
#include "ast.h"

%}

%pure-parser
%locations
%defines
%error-verbose

%lex-param   { yyscan_t scanner }
%parse-param { Language **language }
%parse-param { yyscan_t scanner }

%union {
	char* identifier;
}

%token LanguageToken
%token <identifier> IdentifierToken

%locations   

%start LanguageDeclaration

%%

LanguageDeclaration
	: LanguageToken IdentifierToken '{' '}' { *language = CreateLanguage($2); }
	;

%%