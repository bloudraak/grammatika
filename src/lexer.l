%option reentrant
%option bison-bridge
%option bison-locations
%option 8bit
%option never-interactive
%option nodefault
%option noinput
%option nounput
%option noyywrap
%option noyyalloc
%option noyyrealloc
%option noyyfree
%option warn

%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"  

#define YY_USER_ACTION  { \
    yylloc->first_line = yylloc->last_line; \
    yylloc->first_column = yylloc->last_column; \
    for(int i = 0; yytext[i] != '\0'; i++) { \
        if(yytext[i] == '\n') { \
            yylloc->last_line++; \
            yylloc->last_column = 0; \
        } \
        else { \
            yylloc->last_column++; \
        } \
    }}

%}

%%

"language" 				{ return(LanguageToken); }
[a-zA-Z_]([a-zA-Z0-9_])* 						{ yylval->identifier = strdup(yytext); return(IdentifierToken); }  

";"								{ return(';'); }
"{"								{	return('{'); }
"}"								{	return('}'); }
","								{ return(','); }
":"								{ return(':'); }
"="								{ return('='); }
"("								{ return('('); }
")"								{ return(')'); }
"["								{ return('['); }
"]"								{ return(']'); }
"."								{ return('.'); }
"&"								{ return('&'); }
"!"								{ return('!'); }
"~"								{ return('~'); }
"-"								{ return('-'); }
"+"								{ return('+'); }
"*"								{ return('*'); }
"/"								{ return('/'); }
"%"								{ return('%'); }
"<"								{ return('<'); }
">"								{ return('>'); }
"^"								{ return('^'); }
"|"								{ return('|'); }
"?"								{ return('?'); }

"//"[^\n\r]*			{	/* ignore */ }
[ \t\v\n\f]*			{	/* ignore */ }
.									{ /* ignore */ }

%%  

void *yyalloc(yy_size_t bytes, yyscan_t yyscanner)
{
	return malloc(bytes);
}

void *yyrealloc(void *ptr, yy_size_t bytes, yyscan_t yyscanner)
{
	if (ptr)
		return realloc(ptr, bytes);
	else
		return malloc(bytes);
}

void yyfree(void *ptr, yyscan_t yyscanner)
{
	if (ptr)
		free(ptr);
}