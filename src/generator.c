#include <stdio.h>
#include "ast.h"

void Generate(struct Language* language, const char* path)
{
	FILE* out = NULL;
	if (NULL == path)
	{
		// Add some error handling
		return;
	}

	out = fopen(path, "wt");

	fprintf(out, "namespace %s\n", language->name);	
	fprintf(out, "{\n");
	fprintf(out, "\tusing System;\n");
	fprintf(out, "\tusing System.IO;\n");
	fprintf(out, "\t\t\n");
	fprintf(out, "\tpublic class Parser\n");
	fprintf(out, "\t{\n");
	fprintf(out, "\t\tpublic void Parse(string path)\n");
	fprintf(out, "\t\t{\n");
	fprintf(out, "\t\t\tusing(var stream = File.OpenRead(path))\n");
	fprintf(out, "\t\t\t{\n");
	fprintf(out, "\t\t\t\tParse(stream);\n");
	fprintf(out, "\t\t\t}\n");
	fprintf(out, "\t\t}\n");
	fprintf(out, "\t\t\n");
	fprintf(out, "\t\tpublic void Parse(Stream stream)\n");
	fprintf(out, "\t\t{\n");
	fprintf(out, "\t\t\tusing(var reader = new StreamReader(stream))\n");
	fprintf(out, "\t\t\t{\n");
	fprintf(out, "\t\t\t\tParse(reader);\n");
	fprintf(out, "\t\t\t}\n");
	fprintf(out, "\t\t}\n");
	fprintf(out, "\t\t\n");
	fprintf(out, "\t\tpublic void Parse(TextReader reader)\n");
	fprintf(out, "\t\t{\n");
	fprintf(out, "\t\t\t// Implement the parser here\n");
	fprintf(out, "\t\t}\n");
	fprintf(out, "\t\t\n");
	fprintf(out, "\t}\n");
	fprintf(out, "\n");
	fprintf(out, "\tpublic static class Program\n");
	fprintf(out, "\t{\n");
	fprintf(out, "\t\tstatic void Main(string[] args)\n");
	fprintf(out, "\t\t{\n");
	fprintf(out, "\t\t\tParser parser = new Parser();\n");
	fprintf(out, "\t\t\tparser.Parse(args[0]);\n");
	fprintf(out, "\t\t}\n");
	fprintf(out, "\t}\n");
	fprintf(out, "}\n");

	fclose(out);	
}
