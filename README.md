# Grammatika #

### Background ###

Grammatika means Grammar in Afrikaans. This is a project learning how to write a parser generator that is more geared towards humans, than to a computer. 

### Build ###

To build simply run

		make

### Clean ### 

To clean the outputs run

		make clean